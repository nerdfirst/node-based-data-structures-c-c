#include <iostream>
using namespace std;

struct Node {
	int value;
	struct Node* next;
};

struct Node* head;

struct Node* get(int pos) {
	struct Node *current = head;
	
	int i;
	for (i=0; i<pos; i++) {
		current = current->next;
	}
	
	return current;
}

void insert(int pos, int value) {
	struct Node *newNode = new Node();
	newNode->value = value;
	newNode->next = 0;
	
	if (pos == 0) {
		newNode->next = head;
		head = newNode;
	}
	else {
		struct Node *prev = get(pos-1);
		struct Node *curr = prev->next;

		newNode->next = curr;
		prev->next = newNode;
	}
}

void deleteNode(int pos) {
	if (pos == 0) {
		struct Node *toDelete = head;
		head = toDelete->next;
		delete toDelete;
	}
	else {
		struct Node *prev = get(pos-1);
		struct Node *toDelete = prev->next;
		
		prev->next = toDelete->next;
		delete toDelete;
	}
}

void destroy() {
	struct Node *current = head;
	struct Node *prev;
	while (current != 0) {
		prev = current;
		current = current->next;
		delete prev;
	}
}

void display() {
	struct Node *current = head;
	while (current != 0) {
		cout << "Address: " << current << ", Value: " << current -> value << endl;
		current = current->next;
	}
}

int main() {
	head = 0;
	insert(0, 1);
	insert(1, 2);
	insert(2, 3);
	insert(3, 4);
	insert(0, 5);
	insert(0, 6);
	
	display();
	cout << endl;
	
	deleteNode(0);
	deleteNode(2);
	display();
	
	destroy();
}
