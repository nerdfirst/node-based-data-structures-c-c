#include <stdio.h>

struct Node {
	int value;
	struct Node* next;
};

int main() {
	struct Node third = {3, 0};
	struct Node second = {2, &third};
	struct Node first = {1, &second};
	
	printf("Item 1: %d\n", first.value);
	printf("Item 2: %d\n", first.next->value);
	printf("Item 3: %d\n", first.next->next->value);
	
	return 0;
}