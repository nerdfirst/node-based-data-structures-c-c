#include <iostream>
using namespace std;

template <class T>
class Node {
	private:
		Node* next;
		T value;
		
	public:
		Node(T val) {
			// Constructor
			value = val;
			next = 0;
		}
		Node* getNext() {
			return next;
		}
		T getValue() {
			return value;
		}
		void setNext(Node* n) {
			next = n;
		}
		void setValue(T val) {
			value = val;
		}
};

template <class T>
class LinkedList {
	private:
		Node<T>* head;
		
	public:
		LinkedList() {
			// Constructor
			head = 0;
		}
	
		Node<T>* get(int pos) {
			Node<T> *current = head;
			
			int i;
			for (i=0; i<pos; i++) {
				current = current->getNext();
			}
			
			return current;
		}
		
		void insert(int pos, T value) {
			Node<T> *newNode = new Node<T>(value);
			
			if (pos == 0) {
				newNode->setNext(head);
				head = newNode;
			}
			
			else {
				Node<T> *prev = get(pos-1);
				Node<T> *curr = prev->getNext();
				
				newNode->setNext(curr);
				prev->setNext(newNode);
			}
		}
		
		void deleteNode(int pos) {
			if (pos == 0) {
				Node<T> *toDelete = head;
				head = toDelete->getNext();
				delete toDelete;
			}
			else {
				Node<T> *prev = get(pos-1);
				Node<T> *toDelete = prev->getNext();
				
				prev->setNext(toDelete->getNext());
				delete toDelete;
			}
		}
	
		void display() {
			Node<T> *current = head;
			while (current != 0) {
				cout << "Address: " << current << ", Value: " << current -> getValue() << endl;
				current = current->getNext();
			}
		}
		
		~LinkedList() {
			// Destructor
			Node<T> *current = head;
			Node<T> *prev;
			while (current != 0) {
				prev = current;
				current = current->getNext();
				delete prev;
			}
		}
};

int main() {
	LinkedList<char> ll = LinkedList<char>();
	ll.insert(0, 'a');
	ll.insert(1, 'b');
	ll.insert(2, 'c');
	ll.insert(3, 'd');
	ll.insert(0, 'e');
	ll.insert(0, 'f');
	
	ll.display();
	cout << endl;
	
	ll.deleteNode(0);
	ll.deleteNode(2);
	ll.display();
	
	return 0;
}
