#include <stdio.h>
#include <stdlib.h>

typedef struct TreeNode {
	int data;
	struct TreeNode* left;
	struct TreeNode* right;
} TreeNode;

TreeNode* newNode(int value) {
	TreeNode* node = calloc(1, sizeof(TreeNode));
	node->data = value;
	return node;
}

void add(TreeNode *root, int value) {
	if (root == 0) {
		root = newNode(value);
		return;
	}

	TreeNode *current = root;
	while (1) {
		if (value <= current->data) {
			if (current->left != 0) {
				current = current->left;
			}
			else {
				current->left = newNode(value);
				return;
			}
		}
		else {
			if (current->right != 0) {
				current = current->right;
			}
			else {
				current->right = newNode(value);
				return;
			}
		}
	}
}

void inOrder(TreeNode *node) {
	if (node == 0) {
		return;
	}
	
	inOrder(node->left);
	printf("%d, ", node->data);
	inOrder(node->right);
}

void freeTree(TreeNode *node) {
	if (node == 0) {
		return;
	}
	
	freeTree(node->left);
	freeTree(node->right);
	free(node);
}

int main() {
	TreeNode *root = newNode(8);
	add(root, 2);
	add(root, 7);
	add(root, 6);
	add(root, 9);
	add(root, 5);
	add(root, 1);
	add(root, 4);
	
	inOrder(root);
	freeTree(root);
	
	return 0;
}
