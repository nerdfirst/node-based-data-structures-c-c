#include <iostream>
using namespace std;

class Node {
	private:
		Node* next;
		int value;
		
	public:
		Node(int val) {
			// Constructor
			value = val;
			next = 0;
		}
		Node* getNext() {
			return next;
		}
		int getValue() {
			return value;
		}
		void setNext(Node* n) {
			next = n;
		}
		void setValue(int val) {
			value = val;
		}
};

class LinkedList {
	private:
		Node* head;
		
	public:
		LinkedList() {
			// Constructor
			head = 0;
		}
	
		Node* get(int pos) {
			Node *current = head;
			
			int i;
			for (i=0; i<pos; i++) {
				current = current->getNext();
			}
			
			return current;
		}
		
		void insert(int pos, int value) {
			Node *newNode = new Node(value);
			
			if (pos == 0) {
				newNode->setNext(head);
				head = newNode;
			}
			
			else {
				Node *prev = get(pos-1);
				Node *curr = prev->getNext();
				
				newNode->setNext(curr);
				prev->setNext(newNode);
			}
		}
		
		void deleteNode(int pos) {
			if (pos == 0) {
				Node *toDelete = head;
				head = toDelete->getNext();
				delete toDelete;
			}
			else {
				Node *prev = get(pos-1);
				Node *toDelete = prev->getNext();
				
				prev->setNext(toDelete->getNext());
				delete toDelete;
			}
		}
		
		void display() {
			Node *current = head;
			while (current != 0) {
				cout << "Address: " << current << ", Value: " << current -> getValue() << endl;
				current = current->getNext();
			}
		}
		
		~LinkedList() {
			// Destructor
			Node *current = head;
			Node *prev;
			while (current != 0) {
				prev = current;
				current = current->getNext();
				delete prev;
			}
		}
};

int main() {
	LinkedList ll = LinkedList();
	ll.insert(0, 1);
	ll.insert(1, 2);
	ll.insert(2, 3);
	ll.insert(3, 4);
	ll.insert(0, 5);
	ll.insert(0, 6);
	
	ll.display();
	cout << endl;
	
	ll.deleteNode(0);
	ll.deleteNode(2);
	ll.display();
	
	return 0;
}
