# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Node-Based Data Structures in C / C++**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/ADE1zCQs2Fk/0.jpg)](https://www.youtube.com/watch?v=ADE1zCQs2Fk "Click to Watch")


# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.